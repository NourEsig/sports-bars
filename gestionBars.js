//refs
var dbRefBarsNonValides = firebase.database().ref().child('barsNonValides');
var dbRefBarsValides = firebase.database().ref().child('bars');
var dbRefUserBarsValides = firebase.database().ref().child('user-bars');
var cleBar = document.getElementById('keyBar');
//console.log("Le current user est = "+ firebase.auth().currentUser);
//en écoute
dbRefBarsNonValides.on('value',recupBarsNonValides,errData);
//liste bars non valides
function recupBarsNonValides(data){
	//évite d'afficher en boucle plusieurs fois le le contenu a chaque changement
	if(data != null){
			var lignesBars = $(".barsRow");
		for (var i = 0; i < lignesBars.length; i++) {
			lignesBars[i].remove();
		}
		var barsPasValides = data.val();
		var keys = Object.keys(barsPasValides);
		for (var i = 0; i < keys.length; i++) {
			var cleBar = keys[i];
			var nomBarsAValider = barsPasValides[cleBar].nom;
			var emailBarsAValider = barsPasValides[cleBar].email;
			var adresseBarsAValider = barsPasValides[cleBar].adresse;
			var numeroBarsAValider = barsPasValides[cleBar].numero;
			var row = $("<tr class='barsRow'>");
			  row.append($("<td>"+cleBar+"</td>"))
			     .append($("<td>"+nomBarsAValider+"</td>"))
			     .append($("<td>"+adresseBarsAValider+"</td>"))
			     .append($("<td>"+numeroBarsAValider+"</td>"))
			     .append($("<td>"+emailBarsAValider+"</td>"));
			  $("#listBars").append(row);

			//console.log(i + " " + cleBar+ " " +nomBarsAValider);//affiche la clé
		}
	
	}
}
//en écoute
dbRefBarsValides.on('value',recupBarsValides,errData);
//liste bars valides
function recupBarsValides(data){
	//évite d'afficher en boucle plusieurs fois le le contenu a chaque changement
	var lignesBars = $(".barsRowOk");
	for (var i = 0; i < lignesBars.length; i++) {
		lignesBars[i].remove();
	}

	var barsValides = data.val();
	var keys = Object.keys(barsValides);
	for (var i = 0; i < keys.length; i++) {
		var cleBar = keys[i];
		var nomBarsAValider = barsValides[cleBar].nom;
		var emailBarsAValider = barsValides[cleBar].email;
		var adresseBarsAValider = barsValides[cleBar].adresse;
		var numeroBarsAValider = barsValides[cleBar].numero;
		var row = $("<tr class='barsRowOk'>");
		  row.append($("<td>"+cleBar+"</td>"))
		     .append($("<td>"+nomBarsAValider+"</td>"))
		     .append($("<td>"+adresseBarsAValider+"</td>"))
		     .append($("<td>"+numeroBarsAValider+"</td>"))
		     .append($("<td>"+emailBarsAValider+"</td>"));
		  $("#listBarsAccepte").append(row);

		//console.log(i + "clée bar valide : " + cleBar+ " " +nomBarsAValider);//affiche la clé
		//tbody.append("<tr><th>keys</th> <th><input type='text'>"+nomCompet+"</th><td><button class='btn btn-danger'>Supprimer</button></td></tr>");
	}
}

//refuser bar 
function refuseBar(){
	dbRefBarsNonValides.child(cleBar.value).remove();
	alert("Le bar "+cleBar.value+" a bien été refusé !");
	cleBar.value = "";
}
//accepter bar
function acceptBar(){
	var cle = cleBar.value;
	var dbRefBarsNonValides2 = firebase.database().ref().child('barsNonValides/'+cle);
	dbRefBarsNonValides2.on('value',function (data){
		var barValide = data.val();
		//stocke dans variable
		var nomBarValide = barValide.nom;
		var adresseBarValide = barValide.adresse;
		var numeroBarValide = barValide.numero;
		var emailBarValide = barValide.email;
		var cleBarValide = barValide.cle;
		var uidBarValide = barValide.uid;
		var latBarValide = barValide.lat;
		var lngBarValide = barValide.lng;

		//objet pour bars
		var barData = {
				nom: nomBarValide,
				adresse: adresseBarValide,
				email: emailBarValide,
				numero: numeroBarValide,
				cle: cleBarValide,
				uid: uidBarValide,
				lat: latBarValide,
				lng: lngBarValide
		} 
			
		//création dans firebase
		var updates = {};
		updates['/bars/' + cle] = barData;
		updates['/user-bars/' + uidBarValide + '/' + cle] = barData;

		return firebase.database().ref().update(updates);

		//console.log(nomBarValide);
	},errData);
	//envoi email
	//suppression dans bar non valides
	dbRefBarsNonValides.child(cleBar.value).remove();//supprimer le bar de BarsNonValides
	alert("Le bar "+cleBar.value+" a bien été accepté !");
	cleBar.value = "";
}